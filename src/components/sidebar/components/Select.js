import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

// Chakra imports
import { Flex, Text, Select, Button, Avatar } from "@chakra-ui/react";

// Custom components
import { HSeparator } from "components/separator/Separator";
import { useProjectContext } from "hooks/useProjectContext";

export function SelectProject() {
  const history = useHistory();
  const [options, setOptions] = useState(null);
  const [hash, setHash] = useState(null);
  const { project } = useProjectContext();

  useEffect(() => {
    let h = window.location.hash;
    setHash(h.substring(1));
  });

  return (
    <Flex align="center" direction="column">
      <Flex direction={"column"} alignItems={"center"} height={"120px"}>
        <Flex direction={"row"} alignItems={"center"} height={"90px"}>
          {hash != "/projects" && project && (
            <>
              <Avatar h="60px" w="60px" mr="10px" src={project?.logo} />
              <Text fontSize={"md"} fontWeight="500">
                {project?.name}
              </Text>
            </>
          )}
        </Flex>
        {hash != "/projects" && (
          <Button
            onClick={() => history.push("/projects")}
            mt="20px"
            variant={"light"}
            fontSize="md"
            fontWeight="500"
            borderRadius="70px"
            px="24px"
            py="5px"
          >
            All Projects
          </Button>
        )}
      </Flex>
      {/* {options ? (
        <Select
          fontSize="sm"
          variant="subtle"
          defaultValue="Dinner"
          width="unset"
          ms="-10px"
          fontWeight="700"
        >
          {options.map((opt, index) => (
            <option key={index} value={opt.name}>
              {opt.name}
            </option>
          ))}
        </Select>
      ) : (
        <Text h="26px" w="175px" my="32px" color={"black"}>
          No projects yet
        </Text>
      )} */}
      <HSeparator mt="20px" mb="20px" />
    </Flex>
  );
}

export default SelectProject;
