// Chakra Imports
import {
  Avatar,
  Button,
  Flex,
  Menu,
  MenuButton,
  MenuList,
  useColorModeValue,
} from "@chakra-ui/react";
// Custom Components
import PropTypes from "prop-types";
import React, { useState, useEffect } from "react";
import { SidebarResponsive } from "components/sidebar/Sidebar";
import { useLogout } from "hooks/useLogout";
import { useHistory } from "react-router-dom";
import { UserCell } from "components/popups/UserCell";
// Assets
export default function HeaderLinks(props) {
  const { secondary } = props;
  const { logout } = useLogout();
  const history = useHistory();
  const [user, setUser] = useState([]);

  // Chakra Color Mode
  let menuBg = useColorModeValue("white", "navy.800");
  const shadow = useColorModeValue(
    "14px 17px 40px 4px rgba(112, 144, 176, 0.18)",
    "14px 17px 40px 4px rgba(112, 144, 176, 0.06)"
  );

  const handleLogout = async (e) => {
    e.preventDefault();
    await logout();
    history.push("/auth/sign-in");
  };

  useEffect(() => {
    let data = localStorage.getItem("user_data");
    if (data) {
      let parsed_data = JSON.parse(data);
      setUser(parsed_data);
      console.log("USER CELL: ", parsed_data);
    }
  }, []);

  return (
    <Flex
      w={"100%"}
      alignItems="center"
      flexDirection="row"
      bg={menuBg}
      //   flexWrap={secondary ? { base: "wrap", md: "nowrap" } : "unset"}
      px="30px"
      borderRadius="30px"
      justifyContent={"flex-end"}
    >
      <Flex width={"100%"}>
        <SidebarResponsive />
      </Flex>
      <Menu>
        <MenuButton p="0px">
          <Avatar
            _hover={{ cursor: "pointer" }}
            color="white"
            name={user?.first_name + " " + user?.last_name}
            bg="navy"
            size="sm"
            w="40px"
            h="40px"
          />
        </MenuButton>
        <MenuList
          boxShadow={shadow}
          p="20px"
          mt="5px"
          borderRadius="20px"
          bg={menuBg}
          border="none"
        >
          <Flex w="100%" mb="20px">
            <UserCell user={user} />
          </Flex>
          <Flex flexDirection="column">
            <Button variant="lightBrand" onClick={handleLogout}>
              Sign Out
            </Button>
          </Flex>
        </MenuList>
      </Menu>
    </Flex>
  );
}

HeaderLinks.propTypes = {
  variant: PropTypes.string,
  fixed: PropTypes.bool,
  secondary: PropTypes.bool,
  onOpen: PropTypes.func,
};
